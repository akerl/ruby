# Maintainer: Anatol Pomozov <anatol.pomozov@gmail.com>
# Maintainer: Andreas 'Segaja' Schleifer <segaja at archlinux dot org>
# Maintainer: Tim Meusel <tim@bastelfreak.de>
# Contributor: Thomas Dziedzic <gostrc@gmail.com>
# Contributor: Allan McRae <allan@archlinux.org>
# Contributor: John Proctor <jproctor@prium.net>
# Contributor: Jeramy Rutley <jrutley@gmail.com>
# Contributor: Les Aker <me@lesaker.org>

pkgname=(ruby ruby-docs ruby-stdlib ruby-bundledgems)
pkgver=3.2.2
pkgrel=1
arch=(x86_64)
url='https://www.ruby-lang.org/en/'
license=(BSD custom)
makedepends=(doxygen gdbm graphviz libffi libyaml openssl ttf-dejavu tk)
options=(!emptydirs)
_osslver=3.2.0
source=(https://cache.ruby-lang.org/pub/ruby/${pkgver:0:3}/ruby-${pkgver}.tar.xz
        ruby-openssl-$_osslver.tar.xz::https://github.com/ruby/openssl/archive/refs/tags/v$_osslver.tar.gz)
sha512sums=('a29f24cd80f563f6368952d06d6273f7241a409fa9ab2f60e03dde2ac58ca06bee1750715b6134caebf4c061d3503446dc37a6059e19860bb0010eef34951935'
            '3a47d50ecb7a1547b6168aac88176e560bd2653223cbba2f2dd7cc0cb07f953eb416beb603f031dd2846191db313f975753fefb9b10b7e31a0ef738b00a22726')
b2sums=('8e09fb0f6808d4572f86ea190db4f4b950ff3a13391bf7bc3e515b6d14f356d3f7c1eb5bbbe2de460ef78edee54462fdf9be56722cd9e27a613febfe45f8c40a'
        'a4d93fa62b613464ca99cbd461ecec37c062f1846e2af98c002aea6b987ecf376ca8fad281f0564c471164b6d963f5a5c7d112a09cdc9c713b66943956875d4d')

prepare() {
  cd ruby-${pkgver}
  rm -rf ext/openssl test/openssl
  mv ../openssl-$_osslver/ext/openssl ext
  mv ../openssl-$_osslver/lib ext/openssl
  mv ../openssl-$_osslver/{History.md,openssl.gemspec} ext/openssl
  mv ../openssl-$_osslver/test/openssl test
}

build() {
  cd ruby-${pkgver}

  # this uses malloc_usable_size, which is incompatible with fortification level 3
  export CFLAGS="${CFLAGS/_FORTIFY_SOURCE=3/_FORTIFY_SOURCE=2}"
  export CXXFLAGS="${CXXFLAGS/_FORTIFY_SOURCE=3/_FORTIFY_SOURCE=2}"

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --sharedstatedir=/var/lib \
    --libexecdir=/usr/lib/ruby \
    --enable-shared \
    --disable-rpath \
    --with-dbm-type=gdbm_compat

  make
}

check() {
  cd ruby-${pkgver}

  # this uses malloc_usable_size, which is incompatible with fortification level 3
  export CFLAGS="${CFLAGS/_FORTIFY_SOURCE=3/_FORTIFY_SOURCE=2}"
  export CXXFLAGS="${CXXFLAGS/_FORTIFY_SOURCE=3/_FORTIFY_SOURCE=2}"

  make test
}

package_ruby() {
  pkgdesc='An object-oriented language for quick and easy programming'
  depends=(gdbm openssl libffi libyaml libxcrypt gmp zlib rubygems ruby-stdlib ruby-bundledgems)
  optdepends=(
      'ruby-docs: Ruby documentation'
      'tk: for Ruby/TK'
  )

  cd ruby-${pkgver}

  make DESTDIR="${pkgdir}" install-nodoc

  install -D -m644 COPYING "${pkgdir}/usr/share/licenses/ruby/LICENSE"
  install -D -m644 BSDL "${pkgdir}/usr/share/licenses/ruby/BSDL"

  rubyver=${pkgver:0:3}.0

  # remove rubygems as it shipped as a separate package
  rm -r "${pkgdir}"/usr/lib/ruby/${rubyver}/{rubygems,rubygems.rb}
  rm "${pkgdir}"/usr/bin/gem

  # remove bundler as it shipped as a separate package
  rm "${pkgdir}"/usr/bin/{bundle,bundler}

  # remove bundled rdoc gem
  rm "${pkgdir}"/usr/bin/{rdoc,ri}
  rm "${pkgdir}"/usr/share/man/man1/ri.1

  # remove irb as it is a separate package now
  rm "${pkgdir}"/usr/bin/irb
  rm "${pkgdir}"/usr/share/man/man1/irb.1

  # remove other binaries that are shipped as separate packages
  rm "${pkgdir}"/usr/bin/{rake,rbs,typeprof,erb,racc}
  rm "${pkgdir}"/usr/share/man/man1/erb.1

  # remove all bundled gems to avoid conflicts with ruby-* Arch packages
  rm -r "${pkgdir}"/usr/lib/ruby/gems/${rubyver}/gems/*
  rm "${pkgdir}"/usr/lib/ruby/gems/${rubyver}/specifications/*.gemspec
  rm "${pkgdir}"/usr/lib/ruby/gems/${rubyver}/cache/*.gem

  # remove already packaged stdlib gems (needs to be as dependency in ruby-stdlib)
  local stdlib_gems=(
    abbrev
    base64
    benchmark
    bigdecimal
    bundler
    cgi
    csv
    date
    delegate
    did_you_mean
    digest
    drb
    english
    erb
    etc
    fcntl
    fiddle
    fileutils
    find
    forwardable
    getoptlong
    io-console
    io-nonblock
    io-wait
    ipaddr
    irb
    json
    logger
    mutex_m
    net-http
    open-uri
    psych
    racc
    rdoc
    reline
    stringio
    time
    tmpdir
    uri
  )

  for stdlib_gem in "${stdlib_gems[@]}"; do
    rm --force --recursive --verbose \
      "${pkgdir}"/usr/lib/ruby/${rubyver}/${stdlib_gem} \
      "${pkgdir}"/usr/lib/ruby/${rubyver}/${stdlib_gem}.rb \
      "${pkgdir}"/usr/lib/ruby/${rubyver}/x86_64-linux/${stdlib_gem}.so \
      "${pkgdir}"/usr/lib/ruby/gems/${rubyver}/specifications/default/${stdlib_gem}-*.gemspec
  done

  rm --recursive --verbose \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/English.rb \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/io/console \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/net/http \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/net/http.rb \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/net/https.rb \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/x86_64-linux/cgi \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/x86_64-linux/date_core.so \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/x86_64-linux/digest \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/x86_64-linux/io/console.so \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/x86_64-linux/io/nonblock.so \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/x86_64-linux/io/wait.so \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/x86_64-linux/json \
    "${pkgdir}"/usr/lib/ruby/${rubyver}/x86_64-linux/racc
}

package_ruby-docs() {
  pkgdesc='Documentation files for ruby'

  cd ruby-${pkgver}

  make DESTDIR="${pkgdir}" install-doc install-capi

  install -D -m644 COPYING "${pkgdir}/usr/share/licenses/ruby-docs/LICENSE"
  install -D -m644 BSDL "${pkgdir}/usr/share/licenses/ruby-docs/BSDL"
}

package_ruby-stdlib() {
  # upstream list of gems contained in stdlib ( https://github.com/ruby/ruby/tree/master/{ext,lib} )
  pkgdesc='A vast collection of classes and modules that you can require in your code for additional features'

  depends=(
    ruby-abbrev
    ruby-base64
    ruby-benchmark
    ruby-bigdecimal
    ruby-bundler
    ruby-cgi
    ruby-csv
    ruby-date
    #ruby-dbm   # removed in 3.1.2
    #ruby-debug   # removed in 3.1.2
    ruby-delegate
    ruby-did_you_mean
    ruby-digest
    ruby-drb
    ruby-english
    ruby-erb
    ruby-etc
    ruby-fcntl
    ruby-fiddle
    ruby-fileutils
    ruby-find
    ruby-forwardable
    #ruby-gdbm   # removed in 3.1.2
    ruby-getoptlong
    ruby-io-console
    ruby-io-nonblock
    ruby-io-wait
    ruby-ipaddr
    ruby-irb
    ruby-json
    ruby-logger
    #ruby-matrix   # removed in 3.1.2
    ruby-mutex_m
    #ruby-net-ftp   # removed in 3.1.2
    ruby-net-http
    #ruby-net-imap   # removed in 3.1.2
    #ruby-net-pop   # removed in 3.1.2
    #ruby-net-protocol
    #ruby-net-smtp   # removed in 3.1.2
    #ruby-nkf
    #ruby-observer
    ruby-open-uri
    #ruby-open3
    #ruby-openssl
    #ruby-optparse
    #ruby-ostruct
    #ruby-pathname
    #ruby-pp
    #ruby-prettyprint
    #ruby-prime   # removed in 3.1.2
    #ruby-pstore
    ruby-psych
    ruby-racc
    ruby-rdoc
    #ruby-readline
    #ruby-readline-ext
    ruby-reline
    #ruby-resolv
    #ruby-resolv-replace
    #ruby-rinda
    #ruby-securerandom
    #ruby-set
    #ruby-shellwords
    #ruby-singleton
    ruby-stringio
    #ruby-strscan
    #ruby-syslog
    #ruby-tempfile
    ruby-time
    #ruby-timeout
    ruby-tmpdir
    #ruby-tracer   # removed in 3.1.2
    #ruby-tsort
    #ruby-un
    ruby-uri
    #ruby-weakref
    #ruby-yaml
    #ruby-zlib

    #ruby-error_highlight   # new in 3.2.1
    #ruby2_keywords   # new in 3.1.2 - already exists in [community]
  )
}

package_ruby-bundledgems() {
  # upstream list of bundled gems ( https://github.com/ruby/ruby/blob/master/gems/bundled_gems )
  pkgdesc='Ruby Gems (third-party libraries) that are installed by default when Ruby is installed'

  depends=(
    ruby-minitest
    ruby-power_assert
    ruby-rake
    #ruby-rbs
    ruby-rexml
    #ruby-rss
    ruby-test-unit
    #ruby-typeprof

    # --- new in 3.1.2
    #ruby-debug
    #ruby-matrix
    #ruby-net-ftp
    #ruby-net-imap
    #ruby-net-pop
    #ruby-net-smtp
    #ruby-prime
  )
}

# vim: tabstop=2 shiftwidth=2 expandtab:
